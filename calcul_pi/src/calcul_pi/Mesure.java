package calcul_pi;

import java.util.Random;

public class Mesure implements Runnable{
	private double n_total, n_cible=0, valpi;
	private Random rand = new Random();

	public Mesure(int par_total){
		n_total = par_total;
	}

	private double calcpi(){
		float x,y;
		for(int i=0; i<n_total; i++){
			x = rand.nextFloat();
			y = rand.nextFloat();
			if(Math.pow(x, 2) + Math.pow(y, 2) < 1){
				synchronized (this) {
					n_cible++; //Section critique
				}
			}
		}
		return 4*(n_cible/n_total);
	}

	public void run(){
		valpi = calcpi();
	}

	public double getValpi(){
		return valpi;
	}
}
