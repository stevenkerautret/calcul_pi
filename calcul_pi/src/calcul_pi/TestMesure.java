package calcul_pi;

public class TestMesure {
	private static int nb_points;
	private static int nb_lanceurs;

	public TestMesure(int parPoints, int parLanceurs){
		nb_points = parPoints;
		nb_lanceurs = parLanceurs;
	}

	public double startTest() {
		Mesure[] tabMes = new Mesure[nb_lanceurs];
		Thread[] tabThread = new Thread[nb_lanceurs];
		double sumPi = 0;
		for(int i=0; i<nb_lanceurs; i++){
			tabMes[i] = new Mesure(nb_points);
			tabThread[i] = new Thread(tabMes[i]);
		}
		for(int i=0; i<nb_lanceurs; i++){
			tabThread[i].start();
		}
		for(int i=0; i<nb_lanceurs; i++){
			try{
				tabThread[i].join();
			}catch (Exception e){e.printStackTrace();}
			sumPi += tabMes[i].getValpi();
		}

		return sumPi/nb_lanceurs;
	}

}
