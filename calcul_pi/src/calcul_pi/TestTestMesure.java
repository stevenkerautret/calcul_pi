package calcul_pi;

public class TestTestMesure {
    public static void main(String[] args) {
        int n_tests = 10;
        TestMesure[] testMesures = new TestMesure[n_tests];
        double[] piCalcs = new double[n_tests];
        double pi = Math.PI;
        double[] ecart = new double[n_tests];

        for (int i=0; i<n_tests; i++){
            testMesures[i] = new TestMesure(100000,50);
        }

        for (int i=0; i<n_tests; i++){
            piCalcs[i] = testMesures[i].startTest();

            // Comparaison
            ecart[i] = pi - piCalcs[i];
        }
        double sum = 0;
        for (double d : ecart) sum += Math.abs(d);
        System.out.println("Écart moyen : "+sum/n_tests);
        sum = 0;
        for (double d : piCalcs) sum += Math.abs(d);
        System.out.println("Pi vaut environ : "+sum/n_tests);
    }
}
